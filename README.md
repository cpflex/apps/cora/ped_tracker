# Personnel Tracker Demonstration Application
This Flex application provides basic functions useful for general personnel tracking 
applications in a multi building campus environment, where the tag is worn or stored 
in an RF friendly accessory such as a purse, backpack, jacket pocket.   The application
is feature laden meant as demonstration of CT1000 (Nali N100) capabilities.

**Version: V2.4.6.1a**<br/>
**Release Date: 2024/02/13**

## Key Features  
The tracker application is designed to provide useful tracking and event 
information as an individual goes about their daily activities.
Key features include:

1. Emergency Alert Mode (user and cloud managed)
2. Motion activated location data collection
3. Cloud configurable tracking settings / report
4. Caching of messages when out of range
5. User/Cloud controlled tracking activation / deactivation
6. Battery status indicator
7. Battery status events and report
8. Temperature Monitoring
9. Location measurement configuration control for admin
10. Out of Network Caching (NEW)
11. Improved Network Link Checks

## Settings ##

### Cloud configurable non-volatile Settings
The application supports non-volatile downlink settings for standard reporting interval.  Default reporting intervals are  one (1) minutes for normal mode and five (5) updates for emergency mode. These values survives reboot or dead battery conditions.  

**Note** that the default intervals will change in a future release.  The 1 minute interval normal reporting was chosen to highlight the devices suitability for vehicle tracking.   For actual pedestrian tracking, an interval around 10 minutes is probably a better choice.   Use the downlink commands to changes these values to suit the particular application. 

### Network Link Check Settings
Network link Checks are performed using a combination of time and activity metrics.  Uplink checks are performed
in accordance to the following schedule.   If confirmed messages are sent, the schedule is updated.

|  Link Check Metric                 |      Value            |
|:-----------------------------------|:---------------------:|
|   Stationary Time Interval         |   every 10 minutes    |
|   Stationary Uplink Activity       |   every 3 messages    |
|   Active Tracking Time Interval    |   every 10 minutes    |
|   Active Tracking Uplink Activity  |   every 3 messages    |


These settings can be adjusted by implementing a custom ConfigLinkCheck.i

### Out of Network (OON) Caching Settings
The firmware now supports OON caching of messages, if the link check fails messages marked with the TPF_ARCHIVE
flag, will be stored until the network becomes available.   Once the network is available at DR3 or higher, the
device will push three messages per minute until the archive is empty.  This is in addition to any 
messages stored in the uplink queue, which may contain up to 5 messages.



# User Interface 

## General Operating Mode
General operating mode is entered when the device boots. The tracking mode is on and 
the tag reports location in coarse mode every 5 minutes.  

*Note:  If the tag is on the charger when booting, the entry to general operating mode will be delayed
2 minutes*

**General Operating Mode Button Descriptions**

The following table summarizes the user interface for the application.  See the sections
below for more detailed information regarding each button action.
 
| Button 1 | Button 2 | Description |
|---|---|---|
| very long press (> 3 seconds) |  very long press (> 3 seconds) |  Either button or both, activates emergency mode  (See emergency mode below)|
| 5 quick presses |  5 quick presses |  Either button or both, deactivates emergency mode (See emergency mode below)|
| 1 quick press | | Sends a location report if tracking functions are activated. ***No messages when in privacy mode|
| 3 quick presses | | Activates / Deactivates Privacy Mode (see Tracking below)|
|  | 1 quick press   | Indicates battery level (see Battery Indicator below) |
|  | 2 quick presses | Indicates network coverage status (see Network Indicator below) |
|  | 8 quick presses | Enters location configuration mode (see Location Configuration Mode below). 
| > 15 sec. press | > 15 sec. press | Network Reset the device|
| > 25 sec. press | > 25 sec. press | Factory Reset the device (V2 firmware only)|

**General Operating Mode LED Descriptions** 

The following table indicates the meaning of the LEDs when
not previously triggered by pressing a button.

| LED #1 | LED #2 | Description |
|---|---|---|
| Green blink three times | Green blink three times | Device restarted |
| Green blink three times slowly | | Input valid, may signal alternative depending on function. |
| Red blink three times quickly  | | Input invalid. |
| Flashing Red|Flashing Red| Emergency mode is activating, flashes for three (3) seconds |
| Red blink once every 5 seconds | | Emergency mode is activate
| Flashing Green|Flashing Green| Emergency mode is deactivating, flashes for three (3) seconds|
| Green blink three times slowly   | | Tracking activated.  |
| Red blink four times slowly   | | Tracking deactivated. |
| | Orange slow blink every 10 sec. | Battery is charging. |
| | Green slow blink every 10 sec.  | Battery is fully charged. |
| | Red two(2) blinks every two minutes | Battery is nearly dead, place tag on charger |
| Orange blink five(5) times | Orange blink five(5) times | Enter location configuration mode (see below)|

 
### Battery Indicator 

Quickly pressing Button #2 will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2   | % Charge    | Description|
|----------|-------------|------------|
|  1 red   |   < 5%      | Battery level is critical,  charge immediately. |
|  1 green |  5%  - 30%  | Battery low  less than 30% remaining. |
|  2 green |  30% - 50%  | Battery is about 40% charged. |
|  3 green |  50% - 70%  | Battery is about 60% charged. |
|  4 green |  70% - 90%  | Battery  is about 80% charged. |
|  5 green |  > 90%      | Battery is fully charged. |

The tag will report battery status when connect or disconnect from charging. The battery level and temperature are monitored every 
10 minutes. The tag will also report battery status when its level change more than 5%. The tag will go to low power mode and 
stop automatic location reporting when battery level is below 20%. The tag will resume automatic location reporting after battery 
level return to above 25%. 

## Emergency Mode 

Emergency mode provides a panic button feature, where when the user holds either or both buttons for more than
three (3) seconds, the emergency mode is toggled.  When activated, it sends an emergency activated message to 
the cloud and begins tracking the user. The specified emergency tracking interval is five (5) minutes by default but can be modified by the cloud services.

For this example application, the emergency mode can be deactivated by the user. Hold any button for more than 
three (3) seconds but less than 15 seconds.  The emergency mode will toggle off. In a production version, 
the emergency function should only be deactivated by the cloud service.

## Tracking Mode ##

Users can activate and deactivate the tag tracking function by pressing button #1 three times quickly.
When activated,  the tag will start reporting location at regular intervals while there is significant motion
within the reporting interval.  If the device stops moving significantly for an entire reporting interval,  location 
reporting is suspended until the unit is once again moving. The default reporting interval is one (1) minute. The interval
can be adjusted by the cloud services.

When tracking is activated, the user can report the current location by quickly pressing Button #1.  

## Network status ##
When the tag leaves network coverage area, it will try to connect to the network when there is a message needs 
to be uploaded to the cloud. The tag will save its scanning data when it is out of the network. The saved location data 
will be send to the cloud when the tag comes back to the network area. If the tag is not moving, it will check the 
network status according to loRaWAN standard.

### Network Coverage Indicator 

Quickly pressing Button #2 twice will indicate the status of LoRaWAN network coverage.  

LED #2 will blink as follows to indicate coverage information

| LED #2  | Signal Strength (dBm) | Description                          |
|---------|-----------------------|--------------------------------------|
|  4 green |  -64  to -30    | Very strong signal strength                |
|  3 green |  -89  to  -65   | Good signal strength                       |
|  2 green |  -109  to -90   | Low signal strength                        |
|  1 green |  -120 to -110   | Very low signal strength                   |
|  1 red   |                 | Network unavailable (out of range)         |
|  2 red   |                 | LoRaWAN telemetry disabled. |



### Out of network (OON) / Out of Coverage area ###
When the tag moves out of LoRaWAN network coverage area, it will store at least 2000 location messages in the memory.
When the tag comes back to the network coverage area, it will upload stored messages based on customer requirements. The upload
function will be defined based on customer application.

When the device is out of the network coverage area, it will slow its attempts to reconnect to the network over time to save power. 

RETRY SCHEDULE

| Interval |        **Uplink Offset**       | **Comment**                                                          |
|:--------:|:------------------------------:|----------------------------------------------------------------------|
|  **10**  |   < tLastConfirmedUplink + 60  | Attempt every FLEX_TELEM_DEFAULT_MINUPLINK_SEC seconds for 1 minute. |
|  **120** |  < tLastConfirmedUplink + 1800 | Attempt every 2 minutes for 30 minutes.                              |
|  **600** | < tLastConfirmedUplink + 36000 | Attempt every 10 minutes for 10 hours.                               |
| **1200** | < tLastConfirmedUplink + 86400 | Attempt every 20 minutes for 24 hours.                               |


REJOIN SCHEDULE (Retries first before rejoin)

| **Interval (sec)** |   **state**   | **Time Period (sec)**    | **Comment**                                |
|:------------------:|:-------------:|--------------------------|--------------------------------------------|
|        **0**       | > unavailable | ctCache = 0              | No data pending.                           |
|       **10**       | > unavailable | ctCache > 1              | Minimum uplink interval.                   |
|       **120**      | > disabled    |                          | Cycles no action                           |
|       **30**       | > disabled    | < tUnavailable + 60      | Attempt 2 times for first 60 seconds.      |
|      **1800**      | > disabled    | < tUnavailable + 3600    | Attempt every 30 minutes up to an hour.    |
|      **4200**      | > disabled    | < tUnavailable + 36000   | Attempt every 1.25  hours up to 10  hours. |
|      **28800**     | > disabled    | < tUnavailable + 86,400  | Attempt every 8  hours up to 24 hours.     |
|      **43200**     | > disabled    | < tUnavailable + 604,800 | Attempt every 12  hours up to 1 week.      |
|      **86400**     | > disabled    | > tUnavailable + 604,800 | Attempt every 24 hours until battery dies. |


## Location Configuration Mode (Advanced Use Only)

Enables specification of the location measurement reporting mode.  Device can be configured to test 
various performance and measurement modes.  To enter location configuration mode, press button #2 quickly 
five (5) times.  When mode is activated, the LEDs should blink orange 5 times. To exit location configuration
mode, hold button 1 for two(2) seconds and green LEDs will flash to confirm saving the configuration.

**Location Configuration Mode Button Descriptions**
 
| Button 1 | Button 2 | Description |
|---|---|---|
| 1 long press  |  |  Exit location configuration mode (see LEDs for feedback).
| 1 quick press|  |  Default performance mode (coarse for best battery life) |
| 2 quick press|  |  Coarse performance mode |
| 3 quick press|  |  Medium performance mode |
| 4 quick press|  |  Best performance mode   |
|   | 1 long press | Indicate location mode (performance first, then tech)
|   | 1 quick press | Default measurement mode (WiFi and BLE) |
|   | 2 quick press | WiFi only measurement mode |
|   | 3 quick press | BLE only measurement mode|
|   | 4 quick press | WIFI and BLE measurement mode |

**Location Configuration Mode LED Descriptions** 

| LED #1 | LED #2 | Description |
|---|---|---|
| Green blink five(5) times | Green blink five(5) times | Exit location configuration mode|
| N green blinks| | Indicates performance mode: 1-default, ... 4-best |
| | N green blinks| Indicates measurement mode: 1-default, ... 4-WiFi/BLE |
| Red blink three times | | Input invalid. |

## Settings ##

App version 1.6+ supports non-volatile downlink settings for standard/emergency reporting intervals and accelerometer parameters.

  These values survive reboot or dead battery conditions.


## Source Code ##

The source code for the pedestrian tracker is found in the `src` folder and is organized 
into the following module files:
   * **app.p/app.i** -- main application system events, communications, and UI control.
   * **battery.p/battery.i** -- Battery and power management functions
   * **emergency.p/emergency/i** -- Emergency management functions
   * **tracking.p/tracking.i** -- Tracking controller.
   * **PedTrackerBasic.ocm.json** -- OCM file defines commands and data structures supported by the device.  CP-Flex translates data from the device into these structures to present to the cloud application.
   * **OCMNidDefinitions.i** -- OCM Code Generated NID definitions for CP-Flex communications.   Derived from the PedTrackerBasic.ocm.json
   
To develop your own pedestrian tracker,   copy the entire ped_tracker folder to your own development
area.   Then modify the PedTrackerBasic.ocm.json to define your own application.  Be sure to modify the base nid and uuid. These uniquely define the application and
must not be duplicates.


---
*Copyright 2019-2023, Codepoint Technologies,* 
*All Rights Reserved*
