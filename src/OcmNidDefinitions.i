/*******************************************************************************
*                !!! OCM CODE GENERATED FILE -- DO NOT EDIT !!!
*
* PedTracker Basic Application
*  nid:        PedTrackerBasic
*  uuid:       c175b6d8-540c-4031-8467-9ef05550c6a5
*  ver:        1.5.0.0
*  date:       2022-06-02T00:19:06.257Z
*  product: Nali N100
* 
*  Copyright 2021 - Codepoint Technologies, Inc. All Rights Reserved.
* 
*******************************************************************************/
const OcmNidDefinitions: {
	NID_PedCmdsBatteryCharging = 1,
	NID_PedCmdsBattery = 2,
	NID_PedCmdsConfigNomintvl = 3,
	NID_PedCmdsConfigEmrintvl = 4,
	NID_PedCmdsConfig = 5,
	NID_PedCmdsEmergency = 6,
	NID_PedCmdsBatteryDischarging = 7,
	NID_PedCmdsTrackingDisabled = 8,
	NID_PedCmdsTrackingEnabled = 9,
	NID_PedCmdsTrackingActive = 10,
	NID_PedCmdsEmergencyEnable = 11,
	NID_PedCmds = 12,
	NID_PedCmdsTemperature = 13,
	NID_PedCmdsBatteryCritical = 14,
	NID_PedCmdsConfigInnactivity = 15,
	NID_PedCmdsEmergencyDisable = 16,
	NID_PedCmdsBatterylevel = 17,
	NID_PedCmdsConfigAcquire = 18,
	NID_PedCmdsTracking = 19,
	NID_PedCmdsTelemetry = 20,
	NID_PedCmdsTelemetryPollintval = 21,
	NID_ArchiveSyslogInfo = 22,
	NID_ArchiveSyslogDisable = 23,
	NID_ArchiveErase = 24,
	NID_ArchiveSyslog = 25,
	NID_ArchiveSyslogAlert = 26,
	NID_PedCmdsBatteryCharged = 27,
	NID_ArchiveSyslogAll = 28,
	NID_ArchiveSyslogDetail = 29
};
