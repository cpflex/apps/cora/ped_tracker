﻿# Personnel Tracker Demo Application Release Notes

### V2.4.6.1a - 240213
1. Updated CT1XXX firmware reference to v2.5.3.1a
- Enhanced System Debug Features

### V2.4.6.0a - 240208
1. Updated CT1XXX firmware reference to v2.5.3.0a
- Enhanced Reset Messages

### V2.4.5.1a - 240205
1. Updated CT1XXX firmware reference to v2.5.2.1a

### V2.4.5.0a - 240203
1. Updated CT1XXX firmware reference to v2.5.2.0a
 - Enhanced System Debug Features

### V2.4.4.0a - 240123
1. Updated CT1XXX firmware reference to v2.5.1.0a
 - Bug fixes for BVT / Telem

### V2.4.3.1a - 240116
1. Updated CT1XXX firmware reference to v2.5.0.2a
 - Bug fixes to Archive
 - Changed Telem Unavailable retry from 7 seconds to 30 seconds

### V2.4.3.0a - 240109
1. Updated CT1XXX firmware reference to v2.5.0.0a
 - Fixed various system issues

### V2.4.2.0a - 231206
1. Updated CT1XXX firmware reference to v2.4.4.2a
 - Fixed TimeSync bug (KP-350)
 - Various RTC Kernel Fixes
 - NETWORK RESET operation now rejoins instead of clearing NVM params
2. LoraWAN v1.04 radio stack
  - US915 SB2 / ADR Disabled
  - EU868 Standard Band / ADR enabled

### V2.4.1.0a_104 - 231024
1. Firmware Reference v2.4.1.0a (v1.04 radio stack)
   * Implements refactored FlexTelem with LinkCheck support
   * Default uplink retry scheduler changed
   * After 3 sequential uplink comm failures, TelemState changes to unavailable

### V2.4.0.0a_104 - 231005
1. Firmware Reference v2.4.0.0a
   * LoRaWAN v1.04 radio stack
2. Standard EU band ADR enabled.
3. US915 full band / ADR enabled.

### V2.3.5.0_SENET - 230920
1. Firmware Reference v2.3.0.0_s
   * Incorporates all stabilization improvements.  This is a nearly final release candidate for the firmware.
2. Flex Platform: v1.4.0.2
3. Standard EU band ADR enabled.
4. US915 full band / ADR enabled.

### V2.3.5.0 - 230919
1. Firmware Reference v2.3.0.0
   * Incorporates all stabilization improvements.  This is a nearly final release candidate for the firmware.
2. Flex Platform: v1.4.0.2
3. Standard EU band ADR enabled.

### V2.3.4.0a_senet - 230913 
1. Firmware Reference v2.2.5.0a_s
  - SENET ADR + Full Channel Build
  - Revised LoRaWAN MACReset now clears TX_DELAYED flag
  - Increased Busy Timeout from 15s to 60S  
  - Added Version Report DR0 max 11 byte check

### V2.3.4.0a - 230913 
1. Firmware Reference v2.2.5.0a
   - US915 SB2 / ADR Disabled config
   - Revised LoRaWAN MACReset now clears TX_DELAYED flag
   - Increased Busy Timeout from 15s to 60S  
   - Added Version Report DR0 max 11 byte check

### V2.3.4.1a - 230912 
1. Firmware Reference v2.2.4.0k
  - US915 ADR Enabled with Full Channel Support (Not limited to US915-SB2)

### V2.3.2.0a - 230818
1. Firmware Reference v2.2.2.2k
   - Power fixes.  See firmware release notes for details.

### V2.3.1.0 - 230719
1. Firmware Reference v2.2.1.0
   - Production release of KP_STABLE1

### V2.3.0.3a - 230708
1. Firmware Reference v2.2.0.3k KP_STABLE1
   - Cleanup BVT / Hibernate behavior

### V2.3.0.2a - 230706
1. Firmware Reference v2.2.0.2k KP_STABLE1
   - Many fixes, see firmware notes
   - Uses OPL v1.0.0.4 requires new N100 and Production tools to be compatible

### V2.3.0.1a - 230628
1. Firmware Reference v2.2.0.1k KP_STABLE1
   - Foundation: Fix callback self-invoke bug

### V2.3.0.0a - 230627
1. Firmware Reference v2.2.0.0k KP_STABLE1
   - Foundation Bug fixes: Queue, Callback subsystems
   - Added TimerTick API to PAWN-API

### V2.2.20.2a - 230619
1. Firmware Reference v2.1.5.2k KP_STABLE1
   - Reverts OPL build to v1.0.0.2a to maintain tool compatibility
   - Enables BIST for OPL testing
   - Limited OnCharger log message to 1 hour each
### V2.2.20.1a - 230616
1. Firmware Reference v2.1.5.1k KP_STABLE1
   - New OPL Version 1.0.0.3 fixes USER reset issue
   - Many fixes to system stability, see firmware history for details

### V2.2.19.2a - 230530
1. Test version with LoraWAN EEPROM updates disabled
#### Build V2.2.19.1a - 230517
2. Firmware Reference v2.1.2.1a --
   Reworked Platform Positioning acquisition and complete code in attempt to eliminate cases where it does not transition to IDLE after failure.
   - Restored timers to Kernel_Timer implementation (STM will be on entire time positioning is happening).
   - Removed comm communication failure cases.
   - Changed state update to eliminate potential re-entrancy. 
#### Build V2.2.19.0a - 230515
  1. Firmware reference v2.1.2.0a
     - Added support for hardware Rev4 CT1000 configurations

### V2.2.18.4a, 230511
#### Build V2.2.18.3a, 230510
1. Firmware reference v2.1.1.4a
   - Added Timer recovery & logging
   - Added longer VBat conversion startup delay for testing
#### Build V2.2.18.3a, 230510
1. Firmware reference v2.1.1.3a
   - Reverted I2C memory bus to 100Khz
#### Build V2.2.18.2a, 230508
  1. Firmware reference v2.1.1.2a
     - Changed HAL pre-read / pre-fetch configuration.
#### Build ### V2.2.18.1, 230501
1. Firmware reference v2.1.1.1a
   - Changed I2C memory bus from 100Khz to 400Khz.
   - Fixed accelerometer and i2c memory bugs.
   - Changed BVT LED behavior (manufacturing).

### V2.2.17.1a, V2.2.17.1a_senet 230425
#### build V2.2.17.1a, V2.2.17.1a_senet 230425
1. Updated Firmware bundle to v2.1.0.1a 
   * Bugfix: battery not providing any values (all 0).
    
#### build: V2.2.17.0a, V2.2.17.0a_senet 230424
1. Release is provided to test ADR and 64 channel join requirements for Senet network compliance. 
     Do not use for production use or existing test deployments not supporting ADR.
2. Updated Firmware Bundle to V2.1.0.0a_senet
   - Changed join search to follow standard LoRaWAN US915 region search pattern.
   - Reworked join scheduler and timeouts with respect to restricted mode.
   - Added automatic decrement of datarate when confirmed uplinks timeout more than 4 times in a row.
   - Limited datarates between DR1 and DR4 for all channels in US region.

### V2.2.16.0a 230421
1. Updated Firmware Bundle to V2.0.16.0a
   - Improved battery reporting stability.   

### V2.2.15.0 230417
1. Updated Firmware Bundle to V2.0.15.0a
   - Reworked positioning control to ensure OPL is shutdown to conserve power.
   - Includes updates from alpha bundle v2.0.14.0a.

### V2.2.14.0a 230404
1.  Updated Firmware Bundle to v2.0.14.0a
    - Reworked communications to Join only on network reset.   
    - Added retry schedule of 4 times a day after 1 week.
    - Added protection around the LWAN Busy timeout to reduce thrashing the timer subsystem.  

### V2.2.13.1a 230301 ###
1. References alpha firmware bundle v2.0.13.1a (AU915 supported)
#### build v2.2.13.0a 230228
1. References alpha firmware bundle v2.0.13.0a (AU915 supported) (BROKEN BUILD)

### V2.2.12.0a 230220 ###
1. References alpha firmware bundle v2.0.12.0a (Impulse alpha)

### V2.2.11.0 230208 ###
1. References updated firmware bundle to v2.0.10.4
   - Changes default data rate for EU Region to DR_3.
### V2.2.10.0 230125 ###
1. References updated firmware bundle to v2.0.10.3
   - Adds production support for temperature (not a feature in ped_tracker)
2. References platform SDK version v1.2.13.0
   - Fixes Downlink (health ping) polling interval initialization.

### V2.2.9.6 230118 ###
#### build V2.2.9.6 230118
  1. References updated firmware bundle to v2.0.9.1
     - Hibernation mode power drain bug-fix.  

#### build V2.2.9.5 230103
  1. References updated firmware bundle to v2.0.9.0
     - Fixes foundation bugs.

#### build V2.2.9.4 221223
  1. References updated v2.0.8.4 Bundle fixes bad build from previous. 

#### build V2.2.9.3 221222
  1. References updated v2.0.8.3 Manufacturing BVT bugfixes. 

#### build V2.2.9.2 221222
  1.  References updated v2.0.8.2 Additional manufacturing BVT bugfixes. 

#### build V2.2.9.1 221222
  1.  References updated v2.0.8.1 Bundle supporting manufacturing BVT bugfixes.
  2.  Uses V1 Library v1.2.11.1, which increases motion tracker default sensitivity, now 50mg.

#### build V2.2.9.0a 221217
  1.  Alpha Release Candidate with refactored archive and syslog subsystem.
  2.  References updated v2.0.8.0 Bundle & STM Firmware using the V2 Stack.
  3.  Uses V1 Library v1.2.11.0

### V2.2.8.0a 221129 ###
1)  Alpha test version with debugging enabled to provide base version for additional application improvements under test.

### V2.2.7.0 221129  ###
1)  Restructured build environment to support macros.
2)  Turns off System Logging by default.  Now only enabled for alpha builds since it can create
    potential destabilization under heavy processing conditions using the archive.
3)  This version is meant as a stabile release candidate.
   
### V2.2.6.5 221128 ###
1)  References updated v2.0.7.5 Bundle & STM Firmware using the V2 Stack.
2)  Reverted motion tracking update rate to 1hZ
3)  Added battery monitor debuginfo

### V2.2.6.4 221109 ###
1) Added tFirstFailed to Telemetry Status
1) Added temporary debug messages to MotionTracking.p

### V2.2.6.3 221108 ###
1) Enhanced motionTracking debuginfo

### V2.2.6.2 221107 ###
1)  References updated v2.0.7.3 Bundle & STM Firmware using the V2 Stack.

### V2.2.6.1 221028 ###
1)  References updated v2.0.7.1 Bundle & STM Firmware using the V2 Stack.
2)  Changed Motion Tracking Update rate to 10hZ (improve responsiveness)

### V2.2.6.0 221024 ###
1)  References updated v2.0.7.0 Bundle & STM Firmware using the V2 Stack.
2)  Enable System Logging by default (temporarily enabled for this version)

### V2.2.5.4 221006 ###
- Build 4 (221006)
  * Updated firmware bundle to v2.0.6.4
- Build 3 (221004)
  * Updated firmware bundle to v2.0.6.3
	1. Fixed set Factory Hibernate issue
- Build 2 (221003)
  * Updated firmware bundle to v2.0.6.2
      1. Fixed Authorization issue
      2. Improved reliability or Telemetry Enable/Disable.
- Build 1a (220930)
  * Updated firmware bundle to v2.0.6.1a to included out of network bugfix.

- Build 0a (220929)
  * Reverted System Logging to disabled by default.
  * References updated v2.0.6.0a Bundle & STM Firmware using the V2 Stack.
  * Uses V1 Library v1.2.8.0
   
### V2.2.4.0a 220922 ###
1)  References updated v2.0.5.0a Bundle & STM Firmware using the V2 Stack.
	1) Fixed callback handle exhaustion in certain conditions	

### V2.2.3.0a 220902 ###
1) Fixed lack of LED indicator on Location Acquisition
2) Enable System Logging by default (temporarily enabled for this version)
3) References updated v2.0.4.0a Bundle & STM Firmware using the V2 Stack.
  1) Updated LoRaWAN join schedule to latest
  2) Restored LoRa dutycycle to EU868

### V2.2.2.0a 220824 ###
1) References updated v2.0.3.0a Bundle & STM Firmware using the V2 Stack.
  1. Telemetry Default Scheduler updated
  2. EU868 Minimum TX Data rate changed from D1 to DR3 to match US915 throughput.
  3. Battery Charger IO is now polled every 20 seconds, instead of interrupt based.
  4. Changed Battery Charger IO scheme to tristate Charger IO's when not reading state
  5. Fixed Archive "Empty or Invalid Record" errors when using System Log subsystem.
  6. Updated Class C LoraWAN handling (not applicable to Nali)
  7. LED Locking API added to Platform
  8. Added HUB/BVT App Versioning via app version.h

### V2.2.1.4a 220226 ###
1) Added 10% Battery ratchet up mechanism to PedTracker
2) References v2.0.2.0a Bundle & STM Firmware using the V2 Stack:
   1. Refactored OPL comm into Flex Communications (FlexComm) with standardized API for
   multiple UART support.  Allows non-N100 devices to leverage BVT infrastructure
   in Flex Platform.
   2. Remapped N100 OPL (now FlexComm) command identifiers to align with BVT Commands
   defined by the BVT system to improve portability between devices and consistency.
   Moved all N100 specific commands to separate section  starting at 0x41.
   3. Serial Protocol was updated to support 128 command IDs instead of just 64.     
   4. Added/Enhanced BVT Support.
   5. Enhanced BSP Init Error Handling.
   6. Fixed Helium Sticky MAC bug.
   7. Improved/Fixed Kernel Alarms.
   8. Added Scheduler/BiModal/Battery/Powered Device modules to Foundation.
   9. Implemented Authorization API
   10. Fixed Initial Join continuous restricted issue
3) References V0.9.1.1 OPL Firmware:
   1. Changed Reset UART message handling to support new BVT Reset Scheme.
   2. Added BVT Authorization API.
   Includees all updates from V0.9.1.0
   1. Remapped N100 OPL (now FlexComm) command identifiers to align with BVT Commands
	defined by the BVT system to improve portability between devices and consistency.
	Moved all N100 specific commands to separate section  starting at 0x41.
   2. Serial Protocol was updated to support 128 command IDs instead of just 64.  


### V2.2.1.3a 220106 ###
1) References for v2.0.1.3a Firmware using the V2 Cora Stack.
   i. Merged untethered codebase improvements.
   i. Fixed bugs in Lorawan Telemetry
   i. Fixed Datarate to DR_1 for U.S. Region
   i. Consolidated timer and alarm infrastructure to work under the CmKernel.  Remove other implementations scattered across
   the platform and Pawn-VM.
   i. Added Cora_System APIs.
   i. Added network (10s) and factory reset (20s) options.
   i. Now performs network reset when installing new flex applications.
   i  Fixed duplicate PAWN initialization.
6. Applies all bug fixes in V2.1.3.2 and v1.2.2.4.


### V2.2.0.0a 211008 ###
1) Implemented support for v2.0.0.a Firmware for testing.

---

# V1 Firmware Stack Releases (Deprecated)
The V2.1.X.X builds use V1.X firmware for the Nali-N100.  These releases are deprecated and no longer recommended for production use. 
They are not supported by Codepoint.

### V2.1.7.0 220801a ###
* Updated firmware bundle to v1.0.4.0.
  1. Attempts to fix burn down issue.  No significant functional changes.

### V2.1.6.1 220205 ###
* Updated firmware bundle to v1.0.3.1
   1. Bug fix for Helium runaway uplinks.  Pushing out to verify that it works.
   https://codepoint.atlassian.net/jira/software/projects/KP/boards/5?selectedIssue=KP-251


### V2.1.5.0a 220123 ###

* Updated firmware bundle to v1.0.2.2a
   1. Changed EE868 TX MIN & Default data rate from DR0 to DR3
   1. Changed LoRaWAN DR defaults to be region specific (US915 DR1, EU868 DR3)
   1. Changed EU868 LoRaWAN TX & IDLE & Restricted Duty Cycle timings to 30 seconds
   1. Disabled problematic Lorawan_proc _procMac special error condition checking code
   1. Updated Flex_TelemAttachDefault schedule to harmonize with v2 stack
   1. Changed the reset time from 10 seconds to 15 seconds.
   1. Changed the default very-long press time to 3 seconds. 
2) Updated Emergency Signal Indicators
   1. Now Flashes for three seconds when entering (red) or exiting (green ) emergency mode.

### V2.1.4.0 220118 ###

* Updated firmware bundle to v1.0.2.0a
	1. Fixed minimum data rate issue with US915 region allowing DR0 uplinks.
	2. Fixed minimum archive uplink DR issue, DR1 is now minimum.
	3. Removed unneeded, invalid datarate logic from prior release (item 4).
* Enhanced log messages for power save state change

### V2.1.3.2 220103 ###

* Updated to v1.2.6.1 version of platform with motion tracking improvements: 
   1.  Enabled report for zero measurements. 
   2. Added additional locate when unit stops tracking.
   3.  Bugfix motion tracking initialization.
* Bugfix:  Battery power savings was improperly being enabled at 50% threshold now 15 percent.  Also set the disabled threshold to 25%.

### V2.1.2.4 211117 ###

1) Updated to build v1.0.1.3a_dr1 - Attempts fix on battery issue due to cycling on uplink failure.
2) Updated to v1.2.5.0 version of platform with all location messages now confirmed.
3) Fixes minimum U.S. datarate to DR_1.  Default datarate is now DR_1 for all regions.  
4) Variable (non ADR) datarate solution coming in the future.

### V2.1.1.11 211013 ###
1) Changed number of clicks on location configuration mode to 5 as 
   per instructions, was three (3) previously.
2) Updated to build v1.0.0.31

## V2.1.1.9 -- V2.1.1.10
1) Updates to support N110 firmware.  no additional changes.

### V2.1.1.8  210729 ###
1) Set default reporting interval to 1 minute and emergency interval to 5 minutes.  
2) Rework is in planning to change meaning of emergency mode to continuously report even when not moving.
3) Updated App to compile with v1.2.3.0 of CP-Flex Platform 
4) Built with N100 V1.0.0.29 and N110 V1.0.0.5a firmware bundles. 
   See N100/N110 firmware documentation for additional information.
5) Fixes N110 initialization problems, which were causing bad persistence in PAWN apps.
6) Fixes N110 Communication Lockup after confirmation NACK.
7) Attempted fix to re-enable message archiving.

### V2.1.0.6a  210507 ###
1) Built with V1.0.0.22a N100 firmware bundle. See N100 firmware documentation
  for additional information.
2) Added Charge Complete Message
3) Now supports Ped Tracker API version 1.5.0 with SysLog configuration and Archive Erase.


### V2.0.1.4a  210411 ###
1) Built using V1.0.0.10a N100 firmware bundle. See N100 firmware documentation
  for additional information.
2) Stabilizes archiving, fixes archive corruption issue preventing long-term offline caching
3) Fixes bug in SysLog no longer logging after erase, requiring full reset to get it working again.
4) Bug fixes archive erase causing corruption.
5) Stabilization improvements in Kernel and CP-Flex platform.
6) Fixes battery charge complete issue.
7) Fixes 868 restricted issue.

### V2.0.0.0  210331 ###
1) Built using V1.0.0a N100 firmware bundle. Some improvements are noted below.  See N100 firmware documentation
  for additional information.
2) Updated App to compile with v1.1.1 of CP-Flex Platform 
3) Added Out of network (OON) Caching
4) Added Link Check support
5) Fixed bug sending only confirmed messages, new implementation now supports uncofirmed By
   default confirmation for location measurements are managed by the LinkCheck module.
6) Further stablized Archiving implementation by fixing memory leaks and adding additional error checking.
7) New implementation of the radio uplink processing should improve consistency and reliability.

### V1.7.0.6 210326 ###
1) Updated App to compile with v1.1.0.4 of CP-Flex Platform 
2) Updated to reference firmware bundle v0.15.0.4

### V1.6.1.0a 210225 ###

1) Updated app to compile with v1.0.2.0 version of the CP-Flex Platform
2) Set firmware bundle dependency to v0.14.0.2_a with improved join management logic
3) This firmware attempts to address reports that devices will not rejoin the network under
   certain conditions, particularly in the EU region, where a full reset is required to get it 
   to work again.
4) Refactored App to use new V1 platform standard modules.
5) Added persistence to Location Configuration changes.


### V1.6.0.2A 211207 ###

1) Fixed battery & deferred timer start bugs


### V1.6.0.1A 210207 ###

1) Added NVM parameter support for downlink properties.  These survive reboot/dead battery.
2) battery.p no longer sends duplicate battery_ReportStatus on battery monitor updates.
3) Added 2 minute lockout behavior on boot.  (note:  This is a temporary workaround, pending STM enhancements)
4) Changed Tag report default to 5 minutes.
5) Added NVM parameter support for downlink properties.  These survive reboot/dead battery.
6) battery.p no longer sends duplicate battery_ReportStatus on battery monitor updates.
7) Added 2 minute lockout behavior on boot.  (note:  This is a temporary workaround, pending STM enhancements)
8) Changed Tag report default to 5 minutes.
9) Updated Battery reporting to 5%/2% differentials.
10) Fixed battery * deffered timer start bugs
 
### V1.5.0.0 210108 ###

1) Production release of Ped Tracker, 
2) Modified behavior to reduce tracking interval and to persist tracking state when entering/exiting power saving state.
3) Updated Readme.md documentation.
---
*Copyright 2019-2023, Codepoint Technologies,* 
*All Rights Reserved*
